# Fonasa_Test

Crudtest, donde se exponen los metodo de la API REST FULL. se uso como lenguaje de desarrollo Java con el framework Spring Boot y como base de datos H2(EL script de la estructura y los datos se encuentra dentro del código fuente en una archivo llamado data.sql). Al levantar el proyecto localmente, se crea y se inicializa la bd con datos de prueba automáticamene.

# SWAGGER (Documentación de la API)
La API cuenta con Swagger como medio de documentación relacionada al funcionamiento y descripción de los métodos expuestos por la API, es decir, muestra todo lo necesario para poder consumir dicha API.
Para acceder al swagger de la API, se debe desplegar el micro servicio de forma local. Luego abrir una ventana con el navegador de su preferencia con la siguiente URL:
http://localhost:8080/swagger-ui.html#/persona-info-controller

## Métodos expuestos
Los métodos con los que cuenta la API son los siguientes:
```
http://localhost:8080/personas/{id} get
``` 
Devuelve el paciente de acuerdo al id proporcionado en la url si existe

```
http://localhost:8080/personas post
``` 
crea una nueva persona

```
http://localhost:8080/personas/{id} post
``` 
actualiza los datos de la parsona existente

```
http://localhost:8080/personas/{id} delete
``` 
elimina los dtos de una persona
