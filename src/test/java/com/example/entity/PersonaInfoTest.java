/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entity;

import com.example.enums.HairColourEnum;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author usuario
 */
public class PersonaInfoTest {
    
    public PersonaInfoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class PersonaInfo.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        PersonaInfo instance = new PersonaInfo(1,"omar","guillen","santiago", 123445, HairColourEnum.brown);
        Integer expResult = 1;
        Integer result = instance.getId();
        assertEquals(expResult, result);
    }


}
