/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entity;

import com.example.enums.HairColourEnum;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import org.springframework.format.annotation.NumberFormat;

/**
 *
 * @author usuario
 */
@Entity(name="PERSONA")
public class PersonaInfo implements Serializable{
    
    @Id
    private Integer id;
    
    @NotBlank
    private String name;
    
    @NotBlank
    private String lastName;
    
    @NotBlank
    private String address;
    
    @NumberFormat
    private Integer simplePhoneNumber;
    
    @Enumerated(EnumType.STRING)
    private HairColourEnum hairColour;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSimplePhoneNumber() {
        return simplePhoneNumber;
    }

    public void setSimplePhoneNumber(Integer simplePhoneNumber) {
        this.simplePhoneNumber = simplePhoneNumber;
    }

    public HairColourEnum getHairColour() {
        return hairColour;
    }

    public void setHairColour(HairColourEnum hairColour) {
        this.hairColour = hairColour;
    }

    public PersonaInfo(Integer id, String name, String lastName, String address, Integer simplePhoneNumber, HairColourEnum hairColour) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.simplePhoneNumber = simplePhoneNumber;
        this.hairColour = hairColour;
    }

    public PersonaInfo() {
    }
    
    

    
}
