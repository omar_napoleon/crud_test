package com.example.repository;

import com.example.entity.PersonaInfo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends JpaRepository<PersonaInfo, Integer> {
    
    List<PersonaInfo> findByName(String name);
    
}
