/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

import com.example.entity.PersonaInfo;
import com.example.service.PersonaInfoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author usuario
 */
@Api(value = "CRUD API REST full")
@RestController
public class PersonaInfoController {

    @Autowired
    private PersonaInfoService personaInfoService;

    /**
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "Busca la informaión de una persona a traves de su id", response = PersonaInfo.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Recuperada la informaión de una persona exitosamente", response = PersonaInfo.class)
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @GetMapping(value = "/personas/{id}", produces = {"application/json"})
    public ResponseEntity<PersonaInfo> getPersonaById(
            @ApiParam(value = "Id de la persona", required = true)
            @Valid @PathVariable("id") @NumberFormat Integer id) throws Exception {

        return new ResponseEntity<>(personaInfoService.getPersonaInfoById(id), HttpStatus.OK);

    }

    @ApiOperation(value = "crea la informacion de una persona", response = PersonaInfo.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "persona guardada exitosamente", response = PersonaInfo.class)
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @PostMapping(value = "/personas", produces = {"application/json"})
    public ResponseEntity<PersonaInfo> addPersona(
            @ApiParam(value = "atribustos de la persona a crear", required = true)
            @Valid @RequestBody PersonaInfo body) throws Exception {

        return new ResponseEntity<>(personaInfoService.addPersona(body), HttpStatus.OK);

    }

    @ApiOperation(value = "actualiza la informacion de una persona", response = PersonaInfo.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "persona actualizada exitosamente", response = PersonaInfo.class)
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @PostMapping(value = "/personas/{id}", produces = {"application/json"})
    public ResponseEntity<PersonaInfo> updatePersona(
            @ApiParam(value = "atribustos de la persona a crear", required = true)
            @PathVariable("id") Integer id,
            @Valid @RequestBody PersonaInfo body) throws Exception {

        return new ResponseEntity<>(personaInfoService.updatePersona(body), HttpStatus.OK);

    }

    @ApiOperation(value = "elimina la informacion de una persona", response = PersonaInfo.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "persona elimina exitosamente", response = PersonaInfo.class)
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @DeleteMapping(value = "/personas/{id}", produces = {"application/json"})
    public ResponseEntity<String> deletePersona(
            @ApiParam(value = "atribustos de la persona a crear", required = true)
            @PathVariable("id") Integer id) throws Exception {

        personaInfoService.deletePersona(id);
        ObjectMapper objectMapper = new ObjectMapper();
        return new ResponseEntity<>(objectMapper.writeValueAsString("Ok"), HttpStatus.OK);

    }
}
