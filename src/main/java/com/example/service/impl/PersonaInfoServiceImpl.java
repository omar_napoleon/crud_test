/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service.impl;

import com.example.entity.PersonaInfo;
import com.example.repository.PersonaRepository;
import com.example.service.PersonaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author usuario
 */
@Service
public class PersonaInfoServiceImpl implements PersonaInfoService {

    @Autowired
    private PersonaRepository personaRepository;

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public PersonaInfo getPersonaInfoById(Integer id) throws Exception {

        return personaRepository.findById(id).orElseThrow(()
                -> new IllegalArgumentException("Not found person id: " + id));

    }

    /**
     *
     * @param body
     * @return
     * @throws Exception
     */
    @Override
    public PersonaInfo addPersona(PersonaInfo body) throws Exception {

        return personaRepository.save(body);

    }

    /**
     *
     * @param body
     * @return
     * @throws Exception
     */
    @Override
    public PersonaInfo updatePersona(PersonaInfo body) throws Exception {

        return personaRepository.save(body);

    }

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public void deletePersona(Integer id) throws Exception {

        personaRepository.deleteById(id);

    }

}
