/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.entity.PersonaInfo;

/**
 *
 * @author usuario
 */
public interface PersonaInfoService {

    public PersonaInfo getPersonaInfoById(Integer id) throws Exception;
    
    public PersonaInfo addPersona(PersonaInfo body) throws Exception;
    
    public PersonaInfo updatePersona(PersonaInfo body) throws Exception;
    
    public void deletePersona(Integer id) throws Exception;

}
